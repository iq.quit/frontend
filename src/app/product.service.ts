import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Product } from './product';
import { Observable } from 'rxjs';

@Injectable()
export class ProductService {

  private productUrl = '/api/products';
  private headers: Headers = new Headers({ 'Accepts': 'application/json' });

  constructor(private http: Http) { }

  getProducts(): Observable<Product[]> {
    return this.http
      .get(this.productUrl, {headers: this.headers})
      .map((resp: Response) => resp.json() as Product[]);
  }

}
