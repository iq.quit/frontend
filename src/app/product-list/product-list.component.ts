import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Observable<Product[]>;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products = this.productService.getProducts();
  }

  filterBy(type: string): void {
    this.products = this.productService
      .getProducts()
      .map(res => {
        if (!type) { return res; }
        return res.filter(p => p.rateType === type)
      });
  }

  // assumes a number field comparison
  sortBy(type: string): void {
    this.products = this.productService
      .getProducts()
      .map(res => {
        if (!type) { return res; }
        return res.sort((a, b) => b[type] - a[type]);
      });
  }

}
